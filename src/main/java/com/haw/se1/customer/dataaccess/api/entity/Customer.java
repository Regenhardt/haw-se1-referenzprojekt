package com.haw.se1.customer.dataaccess.api.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.util.Assert;

import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.foundation.common.api.datatype.Gender;
import com.haw.se1.foundation.common.api.datatype.PhoneNumber;
import com.haw.se1.general.dataaccess.api.entity.AbstractEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Represents a customer of the application. Customers can e.g. subscribe to courses.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more
@NoArgsConstructor // generates a default constructor (required by Hibernate)
@AllArgsConstructor // generates a constructor for all fields
@RequiredArgsConstructor // generates a constructor for required (i.e. final and @NonNull) fields
@EqualsAndHashCode(callSuper = true) // generates equals and hashCode methods

@Entity // marks this class as an entity
// default table name: CUSTOMER
public class Customer extends AbstractEntity {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/*
	 * The tables for this class (automatically created from the code by the H2 database embedded in this project) can
	 * be seen in the H2 Console when the application is running: http://localhost:8080/h2-console
	 */

	/* ---- Member Fields ---- */

	@Embedded // causes this field's attributes to be stored in columns within this entity's table
	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Column(unique = true) // adds a uniqueness constraint for this field's column (business key column)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column names for inner attributes (without attribute overrides): see comments inside of this field's type
	private CustomerNumber customerNumber;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Size(min = 1, max = 50) // adds a constraint for this field (checked by Hibernate during saving)
	@Pattern(regexp = "^[a-zA-Z ,.'-]*$") // adds a constraint for this field (checked by Hibernate when saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: FIRST_NAME
	private String firstName;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Size(min = 1, max = 50) // adds a constraint for this field (checked by Hibernate during saving)
	@Pattern(regexp = "^[a-zA-Z ,.'-]*$") // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: LAST_NAME
	private String lastName;

	@Enumerated(EnumType.STRING) // causes the value of this enum-type field to be stored under the enum value's name
	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: GENDER
	private Gender gender;

	@Size(max = 100) // adds a constraint for this field (checked by Hibernate during saving)
	@Email // adds a constraint for this field (checked by Hibernate during saving)
	// default column name: EMAIL
	private String email;

	@ElementCollection( // marks this field as collection of non-entity values (stored in junction table)
			fetch = FetchType.EAGER // loads all children when this entity is loaded (not only when accessing them)
	)
	@Size(max = 3) // adds a constraint for this field (checked by Hibernate during saving)
	@Setter(AccessLevel.NONE) // prevents generation of a public setter for this field by Lombok
	// simple value collection realized by junction table; default table name: CUSTOMER_PHONE_NUMBERS
	private Set<PhoneNumber> phoneNumbers = new HashSet<>();

	@OneToOne( // this entity has one child and the child has only one parent
			cascade = CascadeType.ALL, // also removes the child when this entity is removed
			orphanRemoval = true, // removes the child after being detached from this entity without being re-attached
			mappedBy = "owner" // the field name in the child's type which holds the reference to this entity
	)
	// no column for this field (association mapped by child entity)
	private PremiumAccount premiumAccount;

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

	/**
	 * Adds the given phone number to the customer. If the phone number is already present, nothing happens.
	 * 
	 * @param phoneNumber the phone number to add; must not be <code>null</code>
	 * @return <code>true</code> in case the phone number was added, <code>false</code> otherwise
	 */
	public boolean addPhoneNUmber(PhoneNumber phoneNumber) {
		// check preconditions
		Assert.notNull(phoneNumber, "Parameter 'phoneNumber' must not be null!");

		// check if phone number already present
		boolean phoneNumberAlreadyPresent = phoneNumbers.contains(phoneNumber);

		if (!phoneNumberAlreadyPresent) {
			phoneNumbers.add(phoneNumber);
			return true;
		}

		return false;
	}

	/**
	 * Removes the given phone number from the customer. If the phone number is not present, nothing happens.
	 * 
	 * @param phoneNumber the phone number to remove; must not be <code>null</code>
	 * @return <code>true</code> in case the phone number was removed, <code>false</code> otherwise
	 */
	public boolean removePhoneNumber(PhoneNumber phoneNumber) {
		// check preconditions
		Assert.notNull(phoneNumber, "Parameter 'phoneNumber' must not be null!");

		// check if phone number already present
		boolean phoneNumberAlreadyPresent = phoneNumbers.contains(phoneNumber);

		if (phoneNumberAlreadyPresent) {
			phoneNumbers.remove(phoneNumber);
			return true;
		}

		return false;
	}

}
