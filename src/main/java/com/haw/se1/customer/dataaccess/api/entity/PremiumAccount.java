package com.haw.se1.customer.dataaccess.api.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.springframework.util.Assert;

import com.haw.se1.customer.common.api.datatype.PremiumOption;
import com.haw.se1.general.dataaccess.api.entity.AbstractEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Represents a premium account of a customer. A premium account is a payed subscription which grants exclusive offers
 * and other benefits.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more
@NoArgsConstructor // generates a default constructor (required by Hibernate)
@AllArgsConstructor // generates a constructor for all fields
@RequiredArgsConstructor // generates a constructor for required (i.e. final and @NonNull) fields
@EqualsAndHashCode( // generates equals and hashCode methods
		callSuper = true, // calls the superclass's equals/hashCode before calculating for the fields in this class
		exclude = { "owner" } // avoids StackOverflowError (infinite recursion due to back-reference in parent class)
)

@Entity // marks this class as an entity
// default table name: PREMIUM_ACCOUNT
public class PremiumAccount extends AbstractEntity {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/*
	 * The tables for this class (automatically created from the code by the H2 database embedded in this project) can
	 * be seen in the H2 Console when the application is running: http://localhost:8080/h2-console
	 */

	/* ---- Member Fields ---- */

	@OneToOne( // this entity has one parent and the parent has only one child
			fetch = FetchType.LAZY // only loads parent on access (prevent fetch error -> association is bidirectional)
	)
	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: OWNER_ID
	private Customer owner;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: VALID_TO
	private Date validTo;

	@Enumerated(EnumType.STRING) // causes the values of this enum-type field to be stored under the enum values' names
	@ElementCollection( // marks this field as collection of non-entity values (stored in junction table)
			fetch = FetchType.EAGER // loads all elements when this entity is loaded (not only when accessing them)
	)
	@Setter(AccessLevel.NONE) // prevents generation of a public setter for this field by Lombok
	// simple value collection realized by junction table; default table name: PREMIUM_ACCOUNT_BOOKED_OPTIONS
	private Set<PremiumOption> bookedOptions = new HashSet<>();

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

	/**
	 * Adds the given premium option to the booked options. If the option is already booked, nothing happens.
	 * 
	 * @param option the option to add; must not be <code>null</code>
	 * @return <code>true</code> in case the option was added, <code>false</code> otherwise
	 */
	public boolean addBookedOption(PremiumOption option) {
		// check preconditions
		Assert.notNull(option, "Parameter 'option' must not be null!");

		// check if option is already booked
		boolean optionAlreadyBooked = bookedOptions.contains(option);

		if (!optionAlreadyBooked) {
			bookedOptions.add(option);
			return true;
		}

		return false;
	}

	/**
	 * Removes the given option from the booked options. If the option is not booked yet, nothing happens.
	 * 
	 * @param option the option to remove; must not be <code>null</code>
	 * @return <code>true</code> in case the option was removed, <code>false</code> otherwise
	 */
	public boolean removeBookedOption(PremiumOption option) {
		// check preconditions
		Assert.notNull(option, "Parameter 'option' must not be null!");

		// check if option is already booked
		boolean optionBooked = bookedOptions.contains(option);

		if (optionBooked) {
			bookedOptions.remove(option);
			return true;
		}

		return false;
	}

}
