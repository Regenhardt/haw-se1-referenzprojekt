package com.haw.se1.customer.common.api.exception;

import com.haw.se1.customer.common.api.datatype.CustomerNumber;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Represents the exception when a customer could not be created because he already exists.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type
@Value // generates constructors, getters and more
@EqualsAndHashCode(callSuper = false) // generates equals and hashCode methods
public class CustomerAlreadyExistingException extends Exception {

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Class Fields ---- */

	private static final long serialVersionUID = 1L;

	public static final String CUSTOMER_WITH_CUSTOMER_NUMBER_ALREADY_EXISTS = "Customer with customer number %d already exists.";

	/* ---- Member Fields ---- */

	private final CustomerNumber customerNumber;

	/* ---- Constructors ---- */

	public CustomerAlreadyExistingException(CustomerNumber customerNumber) {
		super(String.format(CUSTOMER_WITH_CUSTOMER_NUMBER_ALREADY_EXISTS, customerNumber.getNumber()));
		this.customerNumber = customerNumber;
	}

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
