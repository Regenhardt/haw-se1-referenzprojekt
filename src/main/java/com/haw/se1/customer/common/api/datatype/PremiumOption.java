package com.haw.se1.customer.common.api.datatype;

/**
 * Represents the bookable options for premium accounts.
 * 
 * @author Arne Busch
 */
public enum PremiumOption {

	/** Provides extended personal support and preferred treatment in queues */
	EXTENDED_SUPPORT,

	/** Allows for immediate cancellation of the contract */
	NO_MINIMUM_CONTRACT_TERM,

	/** Grants access to the VIP area for customers */
	VIP_AREA

}
