package com.haw.se1.customer.common.api.datatype;

import javax.persistence.Embeddable;

import org.springframework.util.Assert;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * <p>
 * Represents a customer number. A customer number consists of digits. The maximum number is 2^31 - 1.
 * </p>
 * <p>
 * This is a value type (like e.g. <code>String</code>), i.e. two instances of this class are considered to be equal if
 * their values are equal (although each instance in fact has its own object identity).
 * </p>
 * <p>
 * Instances of this class are immutable, i.e. their values are assigned during construction and may never be changed
 * from there on.
 * </p>
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type (@Value can't be used, but Hibernate needs an argumentless constructor when deserializing)
@NoArgsConstructor(access = AccessLevel.PRIVATE) // generates a default constructor (required by Hibernate)
@Getter // generates getters for all fields
@EqualsAndHashCode // generates equals and hashCode methods
@ToString // generates toString method

@Embeddable // indicates that the type's attributes can be stored in columns of the owning entity's table
public class CustomerNumber {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Member Fields ---- */

	// default column name: NUMBER
	private Integer number;

	/* ---- Constructors ---- */

	public CustomerNumber(Integer number) {
		// check preconditions
		Assert.notNull(number, "Parameter 'number' must not be null!");

		this.number = number;
	}

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
