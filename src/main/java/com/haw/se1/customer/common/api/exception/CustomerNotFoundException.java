package com.haw.se1.customer.common.api.exception;

import com.haw.se1.customer.common.api.datatype.CustomerNumber;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Represents the exception when a customer could not be found by the given search criteria.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type
@Value // generates constructors, getters and more
@EqualsAndHashCode(callSuper = false) // generates equals and hashCode methods
public class CustomerNotFoundException extends Exception {

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Class Fields ---- */

	private static final long serialVersionUID = 1L;

	public static final String CUSTOMER_WITH_ID_NOT_FOUND_MESSAGE = "Could not find customer with ID %d.";

	public static final String CUSTOMER_WITH_CUSTOMER_NUMBER_NOT_FOUND_MESSAGE = "Could not find customer with customer number %d.";

	/* ---- Member Fields ---- */

	private final Long id;

	private final CustomerNumber customerNumber;

	/* ---- Constructors ---- */

	public CustomerNotFoundException(Long id) {
		super(String.format(CUSTOMER_WITH_ID_NOT_FOUND_MESSAGE, id));
		this.id = id;
		this.customerNumber = null;
	}

	public CustomerNotFoundException(CustomerNumber customerNumber) {
		super(String.format(CUSTOMER_WITH_CUSTOMER_NUMBER_NOT_FOUND_MESSAGE, customerNumber.getNumber()));
		this.id = null;
		this.customerNumber = customerNumber;
	}

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
