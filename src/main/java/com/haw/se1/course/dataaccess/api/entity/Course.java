package com.haw.se1.course.dataaccess.api.entity;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.util.Assert;

import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.foundation.dataaccess.api.entity.Language;
import com.haw.se1.general.dataaccess.api.entity.AbstractEntity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Represents a course for personal education. Customers can subscribe to courses.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more
@NoArgsConstructor // generates a default constructor (required by Hibernate)
@AllArgsConstructor // generates a constructor for all fields
@RequiredArgsConstructor // generates a constructor for required (i.e. final and @NonNull) fields
@EqualsAndHashCode(callSuper = true) // generates equals and hashCode methods

@Entity // marks this class as an entity
// default table name: COURSE
public class Course extends AbstractEntity {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/*
	 * The tables for this class (automatically created from the code by the H2 database embedded in this project) can
	 * be seen in the H2 Console when the application is running: http://localhost:8080/h2-console
	 */

	/* ---- Member Fields ---- */

	@Embedded // causes this field's attributes to be embedded (i.e. stored in columns within this entity's table)
	@AttributeOverride( // replaces the default column names for the embedded attributes by more meaningful ones
			name = "code", // the name of the embedded attribute
			column = @Column(name = "COURSE_NUMBER") // the column name in this entity's table
	)
	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Column(unique = true) // adds a uniqueness constraint for this field's column (business key column)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column names for inner attributes (without attribute overrides): see comments inside of this field's type
	private CourseNumber courseNumber;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Size(min = 1, max = 150) // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: NAME
	private String name;

	@ElementCollection( // marks this field as collection of non-entity values (stored in junction table)
			fetch = FetchType.EAGER // loads all children when this entity is loaded (not only when accessing them)
	)
	@Setter(AccessLevel.NONE) // prevents generation of a public setter for this field by Lombok
	// simple value collection realized by junction table; default table name: COURSE_TAGS
	private Set<String> tags = new HashSet<>();

	@ManyToOne // this entity has one child, but the child can have multiple parents
	// default column name: LANGUAGE_ID
	private Language language;

	@ManyToMany( // this entity can have multiple children and every child can have multiple parents
			fetch = FetchType.EAGER // loads all children when this entity is loaded (not only when accessing them)
	)
	@Size(max = 100) // adds a constraint for this field (checked by Hibernate during saving)
	@Setter(AccessLevel.NONE) // prevents generation of a public setter for this field by Lombok
	// association realized by junction table; default table name: COURSE_PARTICIPANTS
	private Set<Customer> participants = new HashSet<>();

	@OneToMany( // this entity can have multiple children, but every child can have only one parent
			cascade = CascadeType.ALL, // also removes children when this entity is removed
			orphanRemoval = true, // removes children after being detached from this entity without being re-attached
			fetch = FetchType.EAGER // loads all children when this entity is loaded (not only when accessing them)
	)
	@Setter(AccessLevel.NONE) // prevents generation of a public setter for this field by Lombok
	// association realized by junction table; default table name: COURSE_REVIEWS
	private Set<CourseReview> reviews = new HashSet<>();

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

	/**
	 * Adds the given tag to the tag list. If the tag is already present, nothing happens.
	 * 
	 * @param tag the tag to add; must not be <code>null</code>
	 * @return <code>true</code> in case the tag was added, <code>false</code> otherwise
	 */
	public boolean addTag(String tag) {
		// check preconditions
		Assert.notNull(tag, "Parameter 'tag' must not be null!");

		// check if tag is already present
		boolean tagAlreadyPresent = tags.contains(tag);

		if (!tagAlreadyPresent) {
			tags.add(tag);
			return true;
		}

		return false;
	}

	/**
	 * Removes the given tag from the tag list. If the tag is not present yet, nothing happens.
	 * 
	 * @param tag the tag to remove; must not be <code>null</code>
	 * @return <code>true</code> in case the tag was removed, <code>false</code> otherwise
	 */
	public boolean removeTag(String tag) {
		// check preconditions
		Assert.notNull(tag, "Parameter 'tag' must not be null!");

		// check if tag is already present
		boolean tagPresent = tags.contains(tag);

		if (tagPresent) {
			tags.remove(tag);
			return true;
		}

		return false;
	}

	/**
	 * Adds the given participant to the course. If the participant is already enrolled in the course, nothing happens.
	 * 
	 * @param participant the participant to add; must not be <code>null</code> and have a non-null customer number
	 * @return <code>true</code> in case the participant was added, <code>false</code> otherwise
	 */
	public boolean addParticipant(Customer participant) {
		// check preconditions
		Assert.notNull(participant, "Parameter 'participant' must not be null!");
		Assert.notNull(participant.getCustomerNumber(),
				"Parameter 'participant' must have a non-null customer number!");

		// check if participant already in list (identified by unique customer number)
		boolean participantAlreadyEnrolled = participants.stream()
				.anyMatch(c -> c.getCustomerNumber().equals(participant.getCustomerNumber()));

		if (!participantAlreadyEnrolled) {
			participants.add(participant);
			return true;
		}

		return false;
	}

	/**
	 * Removes the given participant from the course. If the participant is not enrolled in the course, nothing happens.
	 * 
	 * @param participant the participant to remove; must not be <code>null</code> and have a non-null customer number
	 * @return <code>true</code> in case the participant was removed, <code>false</code> otherwise
	 */
	public boolean removeParticipant(Customer participant) {
		// check preconditions
		Assert.notNull(participant, "Parameter 'participant' must not be null!");
		Assert.notNull(participant.getCustomerNumber(),
				"Parameter 'participant' must have a non-null customer number!");

		// find course in list (identified by unique course number)
		Optional<Customer> enrolledParticipant = participants.stream()
				.filter(c -> c.getCustomerNumber().equals(participant.getCustomerNumber())).findFirst();

		if (enrolledParticipant.isPresent()) {
			participants.remove(enrolledParticipant.get());
			return true;
		}

		return false;
	}

	/**
	 * Removes all of the participants from the course.
	 */
	public void clearParticipants() {
		participants.clear();
	}

	/**
	 * Adds the given review to the review list. If the review is already in the list, nothing happens.
	 * 
	 * @param review the review to add; must not be <code>null</code>
	 * @return <code>true</code> in case the review was added, <code>false</code> otherwise
	 */
	public boolean addReview(CourseReview review) {
		// check preconditions
		Assert.notNull(review, "Parameter 'review' must not be null!");

		// check if review already in list (identified by unique ID)
		boolean reviewAlreadyAdded = false;

		if (review.getId() != null) {
			reviewAlreadyAdded = reviews.stream().anyMatch(r -> review.getId().equals(r.getId()));
		}

		if (!reviewAlreadyAdded) {
			reviews.add(review);
			return true;
		}

		return false;
	}

	/**
	 * Removes the given review from the review list. If the review is not in the list, nothing happens.
	 * 
	 * @param review the review to remove; must not be <code>null</code> and have a non-null ID
	 * @return <code>true</code> in case the review was removed, <code>false</code> otherwise
	 */
	public boolean removeReview(CourseReview review) {
		// check preconditions
		Assert.notNull(review, "Parameter 'review' must not be null!");
		Assert.notNull(review.getId(), "Parameter 'review' must have a non-null ID!");

		// find review in list (identified by unique ID)
		Optional<CourseReview> reviewToRemove = reviews.stream().filter(r -> review.getId().equals(r.getId()))
				.findFirst();

		if (reviewToRemove.isPresent()) {
			reviews.remove(reviewToRemove.get());
			return true;
		}

		return false;
	}

}
