package com.haw.se1.course.dataaccess.api.entity;

import javax.persistence.Entity;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.haw.se1.general.dataaccess.api.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Represents a review for a course made by one of its subscribed customers. A review contains a rating with an integer
 * value between 1 (lowest) and 5 (highest). Reviews can also include an optional comment.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more
@NoArgsConstructor // generates a default constructor (required by Hibernate)
@AllArgsConstructor // generates a constructor for all fields
@RequiredArgsConstructor // generates a constructor for required (i.e. final and @NonNull) fields
@EqualsAndHashCode(callSuper = true) // generates equals and hashCode methods

@Entity // marks this class as an entity
// default table name: COURSE_REVIEW
public class CourseReview extends AbstractEntity {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/*
	 * The tables for this class (automatically created from the code by the H2 database embedded in this project) can
	 * be seen in the H2 Console when the application is running: http://localhost:8080/h2-console
	 */

	/* ---- Member Fields ---- */

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Size(min = 1, max = 101) // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: REVIEWER
	private String reviewer;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Min(1) // adds a constraint for this field (checked by Hibernate during saving)
	@Max(5) // adds a constraint for this field (checked by Hibernate during saving)
	// default column name: RATING
	private int rating;

	@Size(max = 1000) // adds a constraint for this field (checked by Hibernate during saving)
	// default column name: COMMENT
	private String comment;

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
