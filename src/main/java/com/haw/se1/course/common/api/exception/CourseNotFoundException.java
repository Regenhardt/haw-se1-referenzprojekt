package com.haw.se1.course.common.api.exception;

import com.haw.se1.course.common.api.datatype.CourseNumber;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Represents the exception when a course could not be found by the given search criteria.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type
@Value // generates constructors, getters and more
@EqualsAndHashCode(callSuper = false) // generates equals and hashCode methods
public class CourseNotFoundException extends Exception {

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Class Fields ---- */

	private static final long serialVersionUID = 1L;

	public static final String COURSE_WITH_COURSE_NUMBER_NOT_FOUND_MESSAGE = "Could not find course with course number %s.";

	/* ---- Member Fields ---- */

	private final CourseNumber courseNumber;

	/* ---- Constructors ---- */

	public CourseNotFoundException(CourseNumber courseNumber) {
		super(String.format(COURSE_WITH_COURSE_NUMBER_NOT_FOUND_MESSAGE, courseNumber.getCode()));
		this.courseNumber = courseNumber;
	}

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
