package com.haw.se1.course.logic.api.usecase;

import java.util.List;

import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.common.api.exception.CourseNotFoundException;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.common.api.exception.CustomerNotFoundException;
import com.haw.se1.foundation.common.api.exception.MembershipMailNotSentException;

/**
 * Defines use case functionality for {@link Course} entities.
 * 
 * @author Arne Busch
 */
public interface CourseUseCase {

	/**
	 * Returns all available courses (ordered by name).
	 * 
	 * @return the found courses or an empty list if none were found
	 */
	List<Course> findAllCourses();

	/**
	 * Returns the course with the given course number.
	 * 
	 * @param courseNumber the course's course number; must not be <code>null</code>
	 * @return the found course
	 * @throws CourseNotFoundException in case the course could not be found
	 */
	Course findCourseByCourseNumber(CourseNumber courseNumber) throws CourseNotFoundException;

	/**
	 * Enrolls a customer in a course.
	 * 
	 * @param customerNumber the customer's customer number; must not be <code>null</code>
	 * @param courseNumber   the course number of the course to enroll the customer in; must not be <code>null</code>
	 * @throws CourseNotFoundException in case the course could not be found
	 */
	void addParticipantToCourse(CustomerNumber customerNumber, CourseNumber courseNumber)
			throws CustomerNotFoundException, CourseNotFoundException;

	/**
	 * Transfers all courses of a customer to another customer. The source customer's membership to all of his courses
	 * is canceled and the target customer is enrolled in the respective courses.
	 * 
	 * @param fromCustomerNumber the customer number of the source customer to be removed from his courses; must not be
	 *                           <code>null</code>
	 * @param toCustomerNumber   the customer number of the target customer to get the courses of the source customer;
	 *                           must not be <code>null</code>
	 * @throws CustomerNotFoundException in case one of the cust@Override omers could not be found
	 */
	void transferCourses(CustomerNumber fromCustomerNumber, CustomerNumber toCustomerNumber)
			throws CustomerNotFoundException;

	/**
	 * Cancels a course membership. An e-mail is sent to the former course participant confirming the cancellation. If
	 * the given customer is not member of the provided course, nothing changes.
	 * 
	 * @param customerNumber the customer number of the customer whose membership shall be canceled; must not be
	 *                       <code>null</code>
	 * @param courseNumber   the course number of the course to cancel the membership from; must not be
	 *                       <code>null</code>
	 * @throws CustomerNotFoundException      in case the customer could not be found
	 * @throws CourseNotFoundException        in case the course could not be found
	 * @throws MembershipMailNotSentException in case the e-mail with the membership update could not be sent to the
	 *                                        persons in the waiting list
	 */
	void removeParticipantFromCourse(CustomerNumber customerNumber, CourseNumber courseNumber)
			throws CustomerNotFoundException, CourseNotFoundException, MembershipMailNotSentException;

}
