package com.haw.se1.course.logic.impl.usecase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.common.api.exception.CourseNotFoundException;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.course.dataaccess.api.repo.CourseRepository;
import com.haw.se1.course.logic.api.usecase.CourseUseCase;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.common.api.exception.CustomerNotFoundException;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.exception.MembershipMailNotSentException;
import com.haw.se1.foundation.logic.api.usecase.MailUseCase;

/**
 * Default implementation for {@link CourseUseCase}.
 * 
 * @author Arne Busch
 */
@Service // causes Spring to automatically create a Spring bean for this class which can then be used using @Autowired
public class CourseUseCaseImpl implements CourseUseCase {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private CustomerRepository customerRepository;

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private CourseRepository courseRepository;

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private MailUseCase mailUseCase;

	@Override
	public List<Course> findAllCourses() {
		List<Course> courses = courseRepository.findAll();
		courses.sort((course1, course2) -> course1.getName().compareTo(course2.getName()));
		return courses;
	}

	@Override
	public Course findCourseByCourseNumber(CourseNumber courseNumber) throws CourseNotFoundException {
		// check preconditions
		Assert.notNull(courseNumber, "Parameter 'courseNumber' must not be null!");

		// load entity from DB
		return courseRepository.findByCourseNumber(courseNumber)
				.orElseThrow(() -> new CourseNotFoundException(courseNumber));
	}

	@Override
	public void addParticipantToCourse(CustomerNumber customerNumber, CourseNumber courseNumber)
			throws CustomerNotFoundException, CourseNotFoundException {
		// check preconditions
		Assert.notNull(customerNumber, "Parameter 'customerNumber' must not be null!");
		Assert.notNull(courseNumber, "Parameter 'courseNumber' must not be null!");

		// load entities from DB
		Customer customer = customerRepository.findByCustomerNumber(customerNumber)
				.orElseThrow(() -> new CustomerNotFoundException(customerNumber));
		Course course = courseRepository.findByCourseNumber(courseNumber)
				.orElseThrow(() -> new CourseNotFoundException(courseNumber));

		// perform business logic
		course.addParticipant(customer);

		// store entity in DB (from then on: entity object is observed by Hibernate within current transaction)
		courseRepository.save(course);
	}

	@Override
	public void transferCourses(CustomerNumber fromCustomerNumber, CustomerNumber toCustomerNumber)
			throws CustomerNotFoundException {
		// check preconditions
		Assert.notNull(fromCustomerNumber, "Parameter 'fromCustomerNumber' must not be null!");
		Assert.notNull(toCustomerNumber, "Parameter 'toCustomerNumber' must not be null!");

		// load entities from DB
		Customer fromCustomer = customerRepository.findByCustomerNumber(fromCustomerNumber)
				.orElseThrow(() -> new CustomerNotFoundException(fromCustomerNumber));
		Customer toCustomer = customerRepository.findByCustomerNumber(toCustomerNumber)
				.orElseThrow(() -> new CustomerNotFoundException(toCustomerNumber));

		// perform business logic
		List<Course> fromCustomerCourses = courseRepository.findByParticipantsContaining(fromCustomer);
		fromCustomerCourses.forEach(course -> course.addParticipant(toCustomer));
		fromCustomerCourses.forEach(course -> course.removeParticipant(fromCustomer));

		// store entities in DB (from then on: each entity object is observed by Hibernate within current transaction)
		fromCustomerCourses.forEach(course -> courseRepository.save(course));
	}

	@Override
	public void removeParticipantFromCourse(CustomerNumber customerNumber, CourseNumber courseNumber)
			throws CustomerNotFoundException, CourseNotFoundException, MembershipMailNotSentException {
		// check preconditions
		Assert.notNull(customerNumber, "Parameter 'customerNumber' must not be null!");
		Assert.notNull(courseNumber, "Parameter 'courseNumber' must not be null!");

		// load entities from DB
		Customer customer = customerRepository.findByCustomerNumber(customerNumber)
				.orElseThrow(() -> new CustomerNotFoundException(customerNumber));
		Course course = courseRepository.findByCourseNumber(courseNumber)
				.orElseThrow(() -> new CourseNotFoundException(courseNumber));

		// perform business logic
		boolean participantRemoved = course.removeParticipant(customer);

		if (participantRemoved) {
			// store entity in DB (from then on: entity object is observed by Hibernate within current transaction)
			courseRepository.save(course);
		}

		String customerEmail = customer.getEmail();

		// execute other use case
		boolean mailWasSent = mailUseCase.sendMail(customerEmail, "Course membership cancelled",
				"We regret that you cancelled your membership and hope to see you again.");

		if (!mailWasSent) {
			// do some error handling here (including e.g. transaction rollback, etc.)
			// ...

			throw new MembershipMailNotSentException(customerEmail);
		}

	}

}
