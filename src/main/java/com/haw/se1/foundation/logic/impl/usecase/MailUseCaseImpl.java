package com.haw.se1.foundation.logic.impl.usecase;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.haw.se1.foundation.logic.api.usecase.MailUseCase;

/**
 * Default implementation for {@link MailUseCase}.
 * 
 * @author Arne Busch
 */
@Service // causes Spring to automatically create a Spring bean for this class which can then be used using @Autowired
public class MailUseCaseImpl implements MailUseCase {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	public JavaMailSender mailSender;

	@Override
	public boolean sendMail(String to, String subject, String text) {
		// check preconditions
		Assert.isTrue(EmailValidator.getInstance().isValid(to), "Parameter 'to' must be a valid e-mail address!");
		Assert.notNull(subject, "Parameter 'subject' must not be null!");
		Assert.notNull(text, "Parameter 'text' must not be null!");

		// perform business logic
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(to);
			message.setSubject(subject);
			message.setText(text);
			mailSender.send(message);
		} catch (MailException ex) {
			// do some logging and error handling here
			// ...

			return false;
		}

		return true;
	}

}
