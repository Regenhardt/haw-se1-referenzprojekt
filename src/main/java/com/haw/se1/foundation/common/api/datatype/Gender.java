package com.haw.se1.foundation.common.api.datatype;

/**
 * Represents the possible gender declarations.
 * 
 * @author Arne Busch
 */
public enum Gender {

	/** Male */
	MALE,

	/** Female */
	FEMALE,

	/** Other than male/female (diverse) */
	OTHER,

	/** Gender not known */
	UNKNOWN

}
