package com.haw.se1.foundation.common.api.exception;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Represents the exception when an e-mail containing information about a customer's course membership could not be
 * sent.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type
@Value // generates constructors, getters and more
@EqualsAndHashCode(callSuper = false) // generates equals and hashCode methods
public class MembershipMailNotSentException extends Exception {

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Class Fields ---- */

	private static final long serialVersionUID = 1L;

	public static final String COULD_NOT_SEND_MEMBERSHIP_MAIL = "Could not send membership mail to %s.";

	/* ---- Constructors ---- */

	public MembershipMailNotSentException(String recipient) {
		super(String.format(COULD_NOT_SEND_MEMBERSHIP_MAIL, recipient));
	}

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
