package com.haw.se1.foundation.dataaccess.api.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.haw.se1.general.dataaccess.api.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * Represents a language.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more
@NoArgsConstructor // generates a default constructor (required by Hibernate)
@RequiredArgsConstructor // generates a constructor for required (i.e. final and @NonNull) fields
@EqualsAndHashCode(callSuper = true) // generates equals and hashCode methods

@Entity // marks this class as an entity
// default table name: LANGUAGE
public class Language extends AbstractEntity {

	/*
	 * Details about JPA annotations can be found here: http://www.java2s.com/Tutorials/Java/JPA/index.htm
	 */

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/*
	 * The tables for this class (automatically created from the code by the H2 database embedded in this project) can
	 * be seen in the H2 Console when the application is running: http://localhost:8080/h2-console
	 */

	/* ---- Member Fields ---- */

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@Size(min = 2, max = 2) // adds a constraint for this field (checked by Hibernate during saving)
	@Pattern(regexp = "^[a-z]{2}$") // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: CODE
	private String code;

	@NotNull // adds a constraint for this field (checked by Hibernate during saving)
	@NonNull // marks this field as a required field for Lombok (this info is needed in @RequiredArgsConstructor)
	// default column name: NAME
	private String name;

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
