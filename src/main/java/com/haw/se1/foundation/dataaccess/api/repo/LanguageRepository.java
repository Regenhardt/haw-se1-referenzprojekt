package com.haw.se1.foundation.dataaccess.api.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.haw.se1.foundation.dataaccess.api.entity.Language;

/**
 * Represents a repository for the management of {@link Language} entities in a database.
 * 
 * @author Arne Busch
 */
// important: no class "<repository name>Impl" implementing this interface and being annotated with @Component required
// -> Spring Data automatically creates a Spring bean for this repository which can then be used using @Autowired
public interface LanguageRepository extends JpaRepository<Language, Long> {
}
