package com.haw.se1.ping.logic.api.usecase;

import com.haw.se1.ping.common.api.datatype.PingResponse;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;

/**
 * Contains functionality for the handling of ping requests.
 * 
 * @author Arne Busch
 */
public interface PingUseCase {

	/**
	 * Processes an incoming ping request by storing it and returning a response.
	 * 
	 * @param request the incoming ping request; must not be <code>null</code>
	 * @return the response to the request; will never be <code>null</code>
	 */
	PingResponse ping(PingRequest request);

}
