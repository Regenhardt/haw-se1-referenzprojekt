package com.haw.se1.ping.logic.impl.usecase;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.haw.se1.ping.common.api.datatype.PingResponse;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;
import com.haw.se1.ping.dataaccess.api.repo.PingRequestRepository;
import com.haw.se1.ping.logic.api.usecase.PingUseCase;

/**
 * Default implementation of {@link PingUseCase}.
 * 
 * @author Arne Busch
 */
@Component // causes Spring to create a Spring bean for this class and make it usable with @Autowired
public class PingUseCaseImpl implements PingUseCase {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingRequestRepository pingRequestRepository;

	public PingUseCaseImpl() {
		System.out.println("New PingUseCaseImpl constructed");
	}

	/*
	 * @see PingUseCase#ping(PingRequest)
	 */
	@Override
	public PingResponse ping(PingRequest request) {
		Assert.notNull(request, "Parameter 'request' must not be null");

		if (request.getRequester() == null) {
			request.setRequester("<unknown>");
		}

		if (request.getRequestTime() == null) {
			request.setRequestTime(new Date());
		}

		pingRequestRepository.save(request);

		Long sequence = pingRequestRepository.count();
		return new PingResponse("localhost", new Date(), sequence);
	}

}
