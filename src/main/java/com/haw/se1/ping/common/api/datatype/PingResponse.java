package com.haw.se1.ping.common.api.datatype;

import java.util.Date;

import lombok.EqualsAndHashCode;
import lombok.Value;

/**
 * Represents a response to a ping request.
 * 
 * @author Arne Busch
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent an immutable value type
@Value // generates constructors, getters and more
@EqualsAndHashCode // generates equals and hashCode methods
public class PingResponse {

	/*
	 * Details about Lombok annotations can be found here: https://projectlombok.org
	 */

	/* ---- Member Fields ---- */

	private String responder;

	private Date responseTime;

	private Long sequence;

	/* ---- Constructors ---- */

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

	/* ---- Custom Methods ---- */

}
