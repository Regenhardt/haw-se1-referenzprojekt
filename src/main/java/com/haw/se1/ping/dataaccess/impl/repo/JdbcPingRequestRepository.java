package com.haw.se1.ping.dataaccess.impl.repo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.util.Assert;

import com.haw.se1.ping.dataaccess.api.entity.PingRequest;
import com.haw.se1.ping.dataaccess.api.repo.PingRequestRepository;

/**
 * Implementation of {@link PingRequestRepository} which stores entities in a relational database using JDBC.
 * 
 * @author Arne Busch
 */
public class JdbcPingRequestRepository /* implements PingRequestRepository */ {

	/*
	 * Code taken from:
	 * - https://www.tutorialspoint.com/jdbc/jdbc-insert-records.htm
	 * - https://www.tutorialspoint.com/jdbc/jdbc-update-records.htm
	 */

	// JDBC driver name and database URL
	private static final String JDBC_DRIVER = "org.h2.Driver"; // see application.yml
	private static final String DB_URL = "jdbc:h2:mem:testdb"; // see application.yml
	// Database credentials
	private static final String USER = "sa"; // see application.yml
	private static final String PASS = ""; // see application.yml

	private static long NEXT_ID = 1;

	public JdbcPingRequestRepository() {
		System.out.println("New JdbcPingRequestRepository constructed");
	}

	/*
	 * @see PingRequestRepository#save(PingRequest)
	 */
	public PingRequest save(PingRequest entity) {
		Assert.notNull(entity, "Parameter 'entity' must not be null");

		Connection conn = null;
		Statement stmt = null;

		try {
			// Register JDBC driver
			Class.forName(JDBC_DRIVER);

			// Open a connection
			conn = DriverManager.getConnection(DB_URL, USER, PASS);

			// Execute a query
			stmt = conn.createStatement();

			String sql;

			if (entity.getId() == null) {
				// entry must be created
				entity.setId(NEXT_ID++);
				sql = "insert into PING_REQUEST (ID, REQUESTER, REQUEST_TIME)" //
						+ " values (" //
						+ entity.getId() //
						+ ", '" + entity.getRequester() + "'" //
						+ ", TO_DATE('" + entity.getRequestTime() + "')" //
						+ ")";
			} else {
				// entry must be updated
				sql = "update PING_REQUEST" //
						+ " set" //
						+ " REQUESTER = '" + entity.getRequester() + "'" //
						+ ", REQUEST_TIME = TO_DATE('" + entity.getRequestTime() + "')" //
						+ " where ID = " + entity.getId();
			}

			stmt.executeUpdate(sql);
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null) {
					conn.close();
				}
			} catch (SQLException se) {
				// Do nothing
			}

			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}

		return entity;
	}

	/*
	 * @see PingRequestRepository#deleteAll()
	 */
	public void deleteAll() {
		// TODO Implement
	}

	/*
	 * @see PingRequestRepository#count()
	 */
	public long count() {
		// TODO Implement
		return 0;
	}

}
