package com.haw.se1.ping.dataaccess.impl.repo;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.springframework.util.Assert;

import com.haw.se1.ping.dataaccess.api.entity.PingRequest;
import com.haw.se1.ping.dataaccess.api.repo.PingRequestRepository;

/**
 * Implementation of {@link PingRequestRepository} which simply stores entities in a map held in the application's RAM,
 * i.e. we have a transient database here and no persistent storage. Therefore, this implementation should probably only
 * be used in early development stages and for testing purposes.
 * 
 * @author Arne Busch
 */
public class InMemoryPingRequestRepository /* implements PingRequestRepository */ {

	/** Holds all saved ping requests. */
	private Map<Long, PingRequest> db = new HashMap<>();

	public InMemoryPingRequestRepository() {
		System.out.println("New InMemoryPingRequestRepository constructed");
	}

	/*
	 * @see PingRequestRepository#save(PingRequest)
	 */
	public PingRequest save(PingRequest entity) {
		Assert.notNull(entity, "Parameter 'entity' must not be null");

		if (entity.getId() == null) {
			Long id = db.isEmpty() ? 1 : Collections.max(db.keySet()) + 1;
			entity.setId(id);
		}

		db.put(entity.getId(), entity);
		return entity;
	}

	/*
	 * @see PingRequestRepository#deleteAll()
	 */
	public void deleteAll() {
		db.clear();
	}

	/*
	 * @see PingRequestRepository#count()
	 */
	public long count() {
		return db.size();
	}

}
