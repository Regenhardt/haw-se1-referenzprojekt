package com.haw.se1.ping.facade.api;

import com.haw.se1.ping.common.api.datatype.PingResponse;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;

/**
 * Represents a facade for the system operations for pings which are available from outside the system.
 * 
 * @author Arne Busch
 */
public interface PingFacade {

	/**
	 * Processes an incoming ping request by logging it and returning a response.
	 * 
	 * @param request the incoming ping request
	 * @return the response to the request
	 */
	PingResponse ping(PingRequest request);

}
