package com.haw.se1.ping.facade.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.haw.se1.ping.common.api.datatype.PingResponse;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;
import com.haw.se1.ping.facade.api.PingFacade;
import com.haw.se1.ping.logic.api.usecase.PingUseCase;

/**
 * Default implementation of {@link PingFacade}. This implementation uses REST to provide the defined functionality to
 * outside systems.
 * 
 * @author Arne Busch
 */
@RestController // causes Spring to automatically create a Spring bean for this class which responds to REST calls
@RequestMapping(path = "/ping") // defines which REST calls are handled by this class: http://localhost:8080/ping
@CrossOrigin(origins = "*") // allows cross-origin resource sharing (CORS); prevents failed calls from local web client
public class PingFacadeImpl implements PingFacade {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingUseCase pingUseCase;

	public PingFacadeImpl() {
		System.out.println("New PingFacadeImpl constructed");
	}

	/*
	 * @see PingFacade#ping(PingRequest)
	 */
	@Override
	@GetMapping // causes this method to be called upon HTTP request "GET http://localhost:8080/ping"
	public PingResponse ping(PingRequest request) {
		System.out.println("Received ping at: " + new Date() + "; request data: " + request);

		if (request == null) {
			request = new PingRequest("<unknown>", new Date());
		}

		return pingUseCase.ping(request);
	}

}
