package com.haw.se1;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.course.dataaccess.api.entity.CourseReview;
import com.haw.se1.course.dataaccess.api.repo.CourseRepository;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.common.api.datatype.PremiumOption;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.entity.PremiumAccount;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.datatype.Gender;
import com.haw.se1.foundation.common.api.datatype.PhoneNumber;
import com.haw.se1.foundation.dataaccess.api.entity.Language;
import com.haw.se1.foundation.dataaccess.api.repo.LanguageRepository;

/**
 * Inserts some initial data into the database at startup.
 * 
 * @author Arne Busch
 */
@Component
@Profile("!test") // execute the method "run" only in mode "production" (i.e. when @ActiveProfiles != "test")
public class InitialDataInsertionRunner implements CommandLineRunner {

	@Autowired
	private LanguageRepository languageRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Override
	public void run(String... args) {
		/* ---- Foundation Data ---- */

		// create languages
		Language language = new Language("de", "Deutsch");

		languageRepository.save(language);

		/* ---- Customer Data ---- */

		// create customers
		Customer customer = new Customer(new CustomerNumber(1), "Arne", "Busch", Gender.MALE);
		customer.setEmail("arne.busch@haw-hamburg.de");
		customer.addPhoneNUmber(new PhoneNumber("+49-40-12345678"));

		// create premium accounts
		PremiumAccount premiumAccount = new PremiumAccount(customer, new Date(1893456000000L));
		premiumAccount.addBookedOption(PremiumOption.EXTENDED_SUPPORT);
		premiumAccount.addBookedOption(PremiumOption.VIP_AREA);

		customer.setPremiumAccount(premiumAccount);

		customerRepository.save(customer); // also saves premiumAccount -> composition relation

		/* ---- Course Data ---- */

		// create course reviews
		CourseReview review = new CourseReview("Anonymous User", 4, "Good introduction!");

		// create courses
		Course course = new Course(new CourseNumber("SE1"), "Software Engineering 1");
		course.addTag("SoftwareEngineering");
		course.addTag("SoftwareDevelopment");
		course.setLanguage(language);
		course.addParticipant(customer);
		course.addReview(review);

		courseRepository.save(course); // also saves review -> composition relation
	}

}
