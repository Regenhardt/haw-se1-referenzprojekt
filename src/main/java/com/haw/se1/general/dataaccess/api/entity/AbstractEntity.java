package com.haw.se1.general.dataaccess.api.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

/**
 * Holds attributes for common entities (e.g. the technical ID). It also determines the ID generation strategy and how
 * the entity data is represented as JSON.
 */

// Lombok annotations for auto-generation of constructors/getters/setters/equals etc. in compiled classes (replaces manual boilerplate code)
// the following annotations represent a data-centered type
@Data // generates constructors, getters and more

@MappedSuperclass // required to let all subclasses have separate tables
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id") // avoids redundancy in JSON
public abstract class AbstractEntity {

	/* ---- Member Fields ---- */

	@Id // marks this field as the entity's unique technical ID (primary key) in the database
	@GeneratedValue // lets Hibernate take care of assigning an ID to new database entries
	// default column name: ID
	private Long id;

	/* ---- Getters/Setters ---- */

	/* ---- Overridden Methods ---- */

}
