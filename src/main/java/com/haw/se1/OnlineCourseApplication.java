package com.haw.se1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main application class used for running the application.
 * 
 * @author Arne Busch
 */
@SpringBootApplication // marks this class as a Spring Boot application (with features like application context etc.)
public class OnlineCourseApplication {

	// IMPORTANT: This class must lie in the root application package, i.e. next to or above all other classes/packages
	// Spring component scan only recognizes classes below main application class (the one with @SpringBootApplication)

	/**
	 * Starts the application and keeps it running.
	 * 
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		// prevent running the application with an inappropriate Java version
		JavaVersionUtil.checkJavaRuntimeVersion();

		SpringApplication.run(OnlineCourseApplication.class, args);
	}

}
