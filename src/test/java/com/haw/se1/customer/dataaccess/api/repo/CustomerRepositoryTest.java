package com.haw.se1.customer.dataaccess.api.repo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.foundation.common.api.datatype.Gender;

/**
 * Test class for {@link CustomerRepository}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
public class CustomerRepositoryTest {

	@Autowired
	private CustomerRepository customerRepository;

	private Customer customer1;

	private Customer customer2;

	private Customer customer3;

	@BeforeEach
	public void setUp() {
		// set up fresh test data before each test method execution

		customer1 = new Customer(new CustomerNumber(1), "Jane", "Doe", Gender.FEMALE);
		customer1.setEmail("jane.doe@haw-hamburg.de");
		customerRepository.save(customer1);

		customer2 = new Customer(new CustomerNumber(2), "Jane", "Doe", Gender.OTHER);
		customer2.setEmail("jane.doe@uni-hamburg.de");
		customerRepository.save(customer2);

		customer3 = new Customer(new CustomerNumber(3), "Jane", "Doe", Gender.UNKNOWN);
		customerRepository.save(customer3);
	}

	@AfterEach
	public void tearDown() {
		// clean up test data after each test method execution

		customerRepository.deleteAll();
	}

	@Test
	public void findByCustomerNumber_Success() {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();

		// [WHEN]
		Optional<Customer> loadedCustomer = customerRepository.findByCustomerNumber(customerNumber);

		// [THEN]
		assertThat(loadedCustomer.isPresent()).isTrue();
		assertThat(loadedCustomer.get().getCustomerNumber()).isEqualTo(customerNumber);
	}

	@Test
	public void findByCustomerNumber_Success_EmptyResult() {
		// [GIVEN]
		CustomerNumber customerNumber = new CustomerNumber(9999);

		// [WHEN]
		Optional<Customer> loadedCustomer = customerRepository.findByCustomerNumber(customerNumber);

		// [THEN]
		assertThat(loadedCustomer.isPresent()).isFalse();
	}

	@Test
	public void findByFirstNameAndLastNameAndEmailNotNullOrderByGenderDesc_Success() {
		// [GIVEN]
		String firstName = customer1.getFirstName();
		String lastName = customer1.getLastName();
		CustomerNumber customerNumber1 = customer1.getCustomerNumber();
		CustomerNumber customerNumber2 = customer2.getCustomerNumber();

		// [WHEN]
		List<Customer> loadedCustomers = customerRepository
				.findByFirstNameAndLastNameAndEmailNotNullOrderByGenderDesc(firstName, lastName);

		// [THEN]
		assertThat(loadedCustomers).hasSize(2);
		assertThat(loadedCustomers.get(0).getCustomerNumber()).isEqualTo(customerNumber2);
		assertThat(loadedCustomers.get(1).getCustomerNumber()).isEqualTo(customerNumber1);
	}

	@Test
	public void findByFirstNameAndLastNameAndEmailNotNullOrderByGenderDesc_Success_EmptyResult() {
		// [GIVEN]
		String firstName = "Not";
		String lastName = "Existing";

		// [WHEN]
		List<Customer> loadedCustomers = customerRepository
				.findByFirstNameAndLastNameAndEmailNotNullOrderByGenderDesc(firstName, lastName);

		// [THEN]
		assertThat(loadedCustomers).isEmpty();
	}

	@Test
	public void deleteByCustomerNumber_Success() {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();

		// [WHEN]
		customerRepository.deleteByCustomerNumber(customerNumber);

		// [THEN]
		Optional<Customer> loadedCustomer = customerRepository.findByCustomerNumber(customerNumber);
		assertThat(loadedCustomer.isPresent()).isFalse();
	}

	@Test
	public void deleteByCustomerNumber_Success_NoActualDeletion() {
		// [GIVEN]
		CustomerNumber customerNumber = new CustomerNumber(9999);

		// [WHEN]
		customerRepository.deleteByCustomerNumber(customerNumber);

		// [THEN]
		List<Customer> loadedCustomers = customerRepository.findAll();
		assertThat(loadedCustomers).hasSize(3);
	}

}
