package com.haw.se1.customer.logic.api.usecase;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.datatype.Gender;

/**
 * Test class for {@link CustomerUseCase}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
public class CustomerUseCaseTest {

	@Autowired
	private CustomerUseCase customerUseCase;

	@Autowired
	private CustomerRepository customerRepository;

	private Customer customer;

	@BeforeEach
	public void setUp() {
		// set up fresh test data before each test method execution

		customer = new Customer(new CustomerNumber(1), "Jane", "Doe", Gender.FEMALE);
		customerRepository.save(customer);
	}

	@AfterEach
	public void tearDown() {
		// clean up test data after each test method execution

		customerRepository.deleteAll();
	}

	@Test
	public void findAllCustomers_Success() {
		// [GIVEN]
		CustomerNumber customerNumber = customer.getCustomerNumber();

		// [WHEN]
		List<Customer> loadedCustomers = customerUseCase.findAllCustomers();

		// [THEN]
		assertThat(loadedCustomers).hasSize(1);
		assertThat(loadedCustomers).extracting(Customer::getCustomerNumber).containsOnlyOnce(customerNumber);
	}

	// TODO Add test methods for yet untested methods

}
