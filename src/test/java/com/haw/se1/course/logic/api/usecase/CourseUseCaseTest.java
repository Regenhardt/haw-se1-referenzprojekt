package com.haw.se1.course.logic.api.usecase;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.common.api.exception.CourseNotFoundException;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.course.dataaccess.api.repo.CourseRepository;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.common.api.exception.CustomerNotFoundException;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.datatype.Gender;
import com.haw.se1.foundation.common.api.exception.MembershipMailNotSentException;
import com.haw.se1.foundation.logic.api.usecase.MailUseCase;

/**
 * Test class for {@link CourseUseCase}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
@TestInstance(Lifecycle.PER_CLASS)
public class CourseUseCaseTest {

	@Autowired
	private CourseUseCase courseUseCase;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@MockBean
	private MailUseCase mailUseCase;

	private Course course;

	private Customer customer1;

	private Customer customer2;

	@BeforeAll
	public void setUpAll() {
		// actions to be performed once before execution of first test method

		customer1 = new Customer(new CustomerNumber(1), "Jane", "Doe", Gender.FEMALE);
		customer1.setEmail("jane.doe@haw-hamburg.de");
		customerRepository.save(customer1);

		customer2 = new Customer(new CustomerNumber(2), "John", "Smith", Gender.MALE);
		customer2.setEmail("john.smith@haw-hamburg.de");
		customerRepository.save(customer2);
	}

	@AfterAll
	public void tearDownAll() {
		// actions to be performed once after execution of last test method

		customerRepository.deleteAll();
	}

	@BeforeEach
	public void setUp() {
		// set up fresh test data before each test method execution

		course = new Course(new CourseNumber("SE2"), "Software Engineering 2");
		courseRepository.save(course);
	}

	@AfterEach
	public void tearDown() {
		// clean up test data after each test method execution

		courseRepository.deleteAll(); // must be done first -> integrity constraints (Course references Customer)
	}

	@Test
	public void addParticipantToCourse_Success() throws CustomerNotFoundException, CourseNotFoundException {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = course.getCourseNumber();

		// [WHEN]
		courseUseCase.addParticipantToCourse(customerNumber, courseNumber);

		// [THEN]
		List<Course> loadedCourses = courseRepository.findByParticipantsContaining(customer1);
		assertThat(loadedCourses).hasSize(1);
		assertThat(loadedCourses).extracting(Course::getCourseNumber).containsOnlyOnce(courseNumber);
	}

	@Test
	public void addParticipantToCourse_Fail_CustomerNumberNull() {
		// [GIVEN]
		CustomerNumber customerNumber = null;
		CourseNumber courseNumber = course.getCourseNumber();

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.addParticipantToCourse(customerNumber, courseNumber));
	}

	@Test
	public void addParticipantToCourse_Fail_CourseNumberNull() {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = null;

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.addParticipantToCourse(customerNumber, courseNumber));
	}

	@Test
	public void addParticipantToCourse_Fail_CustomerNotFound() {
		// [GIVEN]
		CustomerNumber customerNumber = new CustomerNumber(9999);
		CourseNumber courseNumber = course.getCourseNumber();

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(CustomerNotFoundException.class)
				.isThrownBy(() -> courseUseCase.addParticipantToCourse(customerNumber, courseNumber))
				.withMessageContaining(
						String.format(CustomerNotFoundException.CUSTOMER_WITH_CUSTOMER_NUMBER_NOT_FOUND_MESSAGE,
								customerNumber.getNumber()));
	}

	@Test
	public void addParticipantToCourse_Fail_CourseNotFound() {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = new CourseNumber("0000");

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(CourseNotFoundException.class)
				.isThrownBy(() -> courseUseCase.addParticipantToCourse(customerNumber, courseNumber))
				.withMessageContaining(String.format(
						CourseNotFoundException.COURSE_WITH_COURSE_NUMBER_NOT_FOUND_MESSAGE, courseNumber.getCode()));
	}

	@Test
	public void transferCourses_Success() throws CustomerNotFoundException {
		// [GIVEN]
		Customer fromCustomer = customer1;
		Customer toCustomer = customer2;
		course.addParticipant(fromCustomer);
		courseRepository.save(course);
		CustomerNumber fromCustomerNumber = fromCustomer.getCustomerNumber();
		CustomerNumber toCustomerNumber = toCustomer.getCustomerNumber();
		String courseName = course.getName();

		// [WHEN]
		courseUseCase.transferCourses(fromCustomerNumber, toCustomerNumber);

		// [THEN]
		List<Course> loadedCoursesOfFromCustomer = courseRepository.findByParticipantsContaining(fromCustomer);
		List<Course> loadedCoursesOfToCustomer = courseRepository.findByParticipantsContaining(toCustomer);
		assertThat(loadedCoursesOfFromCustomer).isEmpty();
		assertThat(loadedCoursesOfToCustomer).hasSize(1);
		assertThat(loadedCoursesOfToCustomer).extracting(Course::getName).containsOnlyOnce(courseName);
	}

	@Test
	public void transferCourses_Fail_FromCustomerNumberNull() {
		// [GIVEN]
		CustomerNumber fromCustomerNumber = null;
		CustomerNumber toCustomerNumber = customer2.getCustomerNumber();

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.transferCourses(fromCustomerNumber, toCustomerNumber));
	}

	@Test
	public void transferCourses_Fail_ToCustomerNumberNull() {
		// [GIVEN]
		CustomerNumber fromCustomerNumber = customer1.getCustomerNumber();
		CustomerNumber toCustomerNumber = null;

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.transferCourses(fromCustomerNumber, toCustomerNumber));
	}

	@Test
	public void transferCourses_Fail_FromCustomerNotFound() {
		// [GIVEN]
		CustomerNumber fromCustomerNumber = new CustomerNumber(9999);
		CustomerNumber toCustomerNumber = customer2.getCustomerNumber();

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(CustomerNotFoundException.class)
				.isThrownBy(() -> courseUseCase.transferCourses(fromCustomerNumber, toCustomerNumber))
				.withMessageContaining(
						String.format(CustomerNotFoundException.CUSTOMER_WITH_CUSTOMER_NUMBER_NOT_FOUND_MESSAGE,
								fromCustomerNumber.getNumber()));
	}

	@Test
	public void transferCourses_Fail_ToCustomerNotFound() {
		// [GIVEN]
		CustomerNumber fromCustomerNumber = customer1.getCustomerNumber();
		CustomerNumber toCustomerNumber = new CustomerNumber(9999);

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(CustomerNotFoundException.class)
				.isThrownBy(() -> courseUseCase.transferCourses(fromCustomerNumber, toCustomerNumber))
				.withMessageContaining(
						String.format(CustomerNotFoundException.CUSTOMER_WITH_CUSTOMER_NUMBER_NOT_FOUND_MESSAGE,
								toCustomerNumber.getNumber()));
	}

	@Test
	public void removeParticipantFromCourse_Success()
			throws CustomerNotFoundException, CourseNotFoundException, MembershipMailNotSentException {
		// [GIVEN]
		course.addParticipant(customer1);
		courseRepository.save(course);
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = course.getCourseNumber();

		// configure mock for MailGateway
		when(mailUseCase.sendMail(anyString(), anyString(), anyString())).thenReturn(true);

		// [WHEN]
		courseUseCase.removeParticipantFromCourse(customerNumber, courseNumber);

		// [THEN]
		List<Course> loadedCourses = courseRepository.findByParticipantsContaining(customer1);
		assertThat(loadedCourses).isEmpty();

		// check that MailGateway was called
		// ...
	}

	@Test
	public void removeParticipantFromCourse_Success_BDDStyle()
			throws CustomerNotFoundException, CourseNotFoundException, MembershipMailNotSentException {
		// [GIVEN]
		course.addParticipant(customer1);
		courseRepository.save(course);
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = course.getCourseNumber();

		// configure mock for MailGateway in Behavior Driven Development (BDD) style
		given(mailUseCase.sendMail(anyString(), anyString(), anyString())).willReturn(true);

		// [WHEN]
		courseUseCase.removeParticipantFromCourse(customerNumber, courseNumber);

		// [THEN]
		List<Course> loadedCourses = courseRepository.findByParticipantsContaining(customer1);
		assertThat(loadedCourses).isEmpty();

		// check that MailGateway was called
		// ...
	}

	@Test
	public void removeParticipantFromCourse_Fail_CustomerNumberNull() {
		// [GIVEN]
		CustomerNumber customerNumber = null;
		CourseNumber courseNumber = course.getCourseNumber();

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.removeParticipantFromCourse(customerNumber, courseNumber));
	}

	@Test
	public void removeParticipantFromCourse_Fail_CourseNumberNull() {
		// [GIVEN]
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = null;

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(IllegalArgumentException.class)
				.isThrownBy(() -> courseUseCase.removeParticipantFromCourse(customerNumber, courseNumber));
	}

	@Test
	public void removeParticipantFromCourse_Fail_MailNotSent() {
		// [GIVEN]
		course.addParticipant(customer1);
		courseRepository.save(course);
		CustomerNumber customerNumber = customer1.getCustomerNumber();
		CourseNumber courseNumber = course.getCourseNumber();

		// configure mock for MailGateway
		when(mailUseCase.sendMail(anyString(), anyString(), anyString())).thenReturn(false);

		// [WHEN]
		// [THEN]
		assertThatExceptionOfType(MembershipMailNotSentException.class)
				.isThrownBy(() -> courseUseCase.removeParticipantFromCourse(customerNumber, courseNumber))
				.withMessageContaining(String.format(MembershipMailNotSentException.COULD_NOT_SEND_MEMBERSHIP_MAIL,
						customer1.getEmail()));
	}

}
