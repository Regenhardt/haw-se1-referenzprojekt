package com.haw.se1.course.logic.api.usecase;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.common.api.exception.CourseNotFoundException;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.course.dataaccess.api.repo.CourseRepository;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.common.api.exception.CustomerNotFoundException;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.datatype.Gender;
import com.haw.se1.foundation.dataaccess.api.entity.Language;
import com.haw.se1.foundation.dataaccess.api.repo.LanguageRepository;
import com.haw.se1.foundation.logic.api.usecase.MailUseCase;

/**
 * Test class for use case scenarios in the course component. The test cases here include more complex interaction with
 * the application core and simulate e.g. use case calls in a way which is similar to how these calls would be made from
 * a GUI when a user goes through a use case scenario.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
@TestInstance(Lifecycle.PER_CLASS)
public class CourseScenarioTest {

	@Autowired
	private CourseUseCase courseUseCase;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private LanguageRepository languageRepository;

	@MockBean
	private MailUseCase mailUseCase;

	private Course course1;

	private Course course2;

	private Customer customer1;

	private Language language1;

	@BeforeAll
	public void setUpAll() {
		// actions to be performed once before execution of first test method

		customer1 = new Customer(new CustomerNumber(1), "Jane", "Doe", Gender.FEMALE);
		customer1.setEmail("jane.doe@haw-hamburg.de");
		customerRepository.save(customer1);

		language1 = new Language("de", "Deutsch");
		languageRepository.save(language1);
	}

	@AfterAll
	public void tearDownAll() {
		// actions to be performed once after execution of last test method

		customerRepository.deleteAll();
		languageRepository.deleteAll();
	}

	@BeforeEach
	public void setUp() {
		// set up fresh test data before each test method execution

		course1 = new Course(new CourseNumber("SE2"), "Software Engineering 2");
		course1.setLanguage(language1);
		course1.addTag("SoftwareEngineering");
		course1.addTag("ProjectManagement");
		courseRepository.save(course1);

		course2 = new Course(new CourseNumber("AI"), "Architektur f�r Informationssysteme");
		course2.setLanguage(language1);
		course2.addTag("SoftwareEngineering");
		course2.addTag("Softwarearchitektur");
		courseRepository.save(course2);
	}

	@AfterEach
	public void tearDown() {
		// clean up test data after each test method execution

		courseRepository.deleteAll();
	}

	@Test
	public void bookCourse_Success() throws CustomerNotFoundException, CourseNotFoundException {
		// [GIVEN]
		System.out.println("");
		System.out.println("#### UC-1: Kurs buchen");
		System.out.println("");

		CourseNumber courseNumber = course1.getCourseNumber();
		CustomerNumber customerNumber = customer1.getCustomerNumber();

		// [WHEN]
		// Vorbedingungen:
		// - Der Kunde ist angemeldet
		// - Der Kunde befindet sich auf der Startseite
		// - Der Kunde hat den Kurs noch nicht gebucht
		System.out.println("Startseite");
		System.out.println("  Hauptmen�: [Kurs�bersicht] [Benutzerprofil]");
		System.out.println("");

		// 1. Der Kunde klickt auf "Kurs�bersicht"
		System.err.println("<Klick auf 'Kurs�bersicht'>");
		System.err.println("");

		// 2. Das System l�dt alle Kurse und zeigt diese als Liste an (nach Name geordnet)
		// load courses
		List<Course> courses = courseUseCase.findAllCourses();
		// display courses
		System.out.println("Kurs�bersicht");
		courses.forEach(
				course -> System.out.println("  [" + course.getCourseNumber().getCode() + "] " + course.getName()));
		System.out.println("");

		// 3. Der Kunde klickt auf einen Kurs
		System.err.println("<Klick auf '" + courseNumber.getCode() + "'>");
		System.err.println("");

		// 4. Das System l�dt den gew�hlten Kurs und zeigt dessen Kursseite an
		// load course
		Course selectedCourse = courseUseCase.findCourseByCourseNumber(courseNumber);
		// display course
		System.out.println("Kurs '" + selectedCourse.getCourseNumber().getCode() + "'");
		System.out.println("  Name: " + selectedCourse.getName());
		System.out.println(
				"  Sprache: " + Optional.ofNullable(selectedCourse.getLanguage()).map(Language::getName).orElse(""));
		System.out.println("  Tags: " + selectedCourse.getTags().stream().collect(Collectors.joining(" ")));
		System.out.println("  [Zur�ck] [Kurs buchen]");
		System.out.println("");

		// 5. Der Kunde klickt auf "Kurs buchen"
		System.err.println("<Klick auf 'Kurs buchen'>");
		System.err.println("");

		// 6. Das System zeigt einen Best�tigungsdialog an
		System.out.println("Wollen Sie den Kurs wirklich buchen?");
		System.out.println("  [Ja] [Nein]");
		System.out.println("");

		// 7. Der Kunde best�tigt die Kursbuchung
		System.err.println("<Klick auf 'Ja'>");
		System.err.println("");

		// 8. Das System f�gt den Kunden dem Kurs als Teilnehmer hinzu
		courseUseCase.addParticipantToCourse(customerNumber, selectedCourse.getCourseNumber());

		// 9. Das System zeigt eine Best�tigungsmeldung f�r die Buchung an
		System.out.println("Sie wurden dem Kurs als Teilnehmer hinzugef�gt.");
		System.out.println("");

		// [THEN]
		Optional<Course> loadedCourse = courseRepository.findByCourseNumber(selectedCourse.getCourseNumber());
		assertThat(loadedCourse.get().getParticipants()).extracting(Customer::getCustomerNumber)
				.contains(customerNumber);
	}

}
