package com.haw.se1.course.dataaccess.api.repo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.course.common.api.datatype.CourseNumber;
import com.haw.se1.course.dataaccess.api.entity.Course;
import com.haw.se1.customer.common.api.datatype.CustomerNumber;
import com.haw.se1.customer.dataaccess.api.entity.Customer;
import com.haw.se1.customer.dataaccess.api.repo.CustomerRepository;
import com.haw.se1.foundation.common.api.datatype.Gender;

/**
 * Test class for {@link CourseRepository}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
@TestInstance(Lifecycle.PER_CLASS)
public class CourseRepositoryTest {

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private CustomerRepository customerRepository;

	private Course course1;

	private Course course2;

	private Customer customer1;

	private Customer customer2;

	@BeforeAll
	public void setUpAll() {
		// actions to be performed once before execution of first test method

		customer1 = new Customer(new CustomerNumber(1), "Jane", "Doe", Gender.FEMALE);
		customerRepository.save(customer1);

		customer2 = new Customer(new CustomerNumber(2), "Luca", "Lotti", Gender.OTHER);
		customerRepository.save(customer2);
	}

	@AfterAll
	public void tearDownAll() {
		// actions to be performed once after execution of last test method

		customerRepository.deleteAll();
	}

	@BeforeEach
	public void setUp() {
		// set up fresh test data before each test method execution

		// SE2 for study course "Angewandte Informatik"
		course1 = new Course(new CourseNumber("SE2"), "Software Engineering 2");
		course1.addParticipant(customer1);
		courseRepository.save(course1);

		// SE2 for study course "Wirtschaftsinformatik"
		course2 = new Course(new CourseNumber("SE2WI"), "Software Engineering 2");
		course2.addParticipant(customer1);
		courseRepository.save(course2);
	}

	@AfterEach
	public void tearDown() {
		// clean up test data after each test method execution

		courseRepository.deleteAll(); // must be done first -> integrity constraints (Course references Customer)
	}

	@Test
	public void findByCourseNumber_Success() {
		// [GIVEN]
		CourseNumber courseNumber = course1.getCourseNumber();

		// [WHEN]
		Optional<Course> loadedCourse = courseRepository.findByCourseNumber(courseNumber);

		// [THEN]
		assertThat(loadedCourse.isPresent()).isTrue();
		assertThat(loadedCourse.get().getCourseNumber()).isEqualTo(courseNumber);
	}

	@Test
	public void findByCourseNumber_Success_EmptyResult() {
		// [GIVEN]
		CourseNumber courseNumber = new CourseNumber("0000");

		// [WHEN]
		Optional<Course> loadedCourse = courseRepository.findByCourseNumber(courseNumber);

		// [THEN]
		assertThat(loadedCourse.isPresent()).isFalse();
	}

	@Test
	public void findByName_Success() {
		// [GIVEN]
		String courseName = course1.getName();
		CourseNumber courseNumber1 = course1.getCourseNumber();
		CourseNumber courseNumber2 = course2.getCourseNumber();

		// [WHEN]
		List<Course> loadedCourses = courseRepository.findByName(courseName);

		// [THEN]
		assertThat(loadedCourses).hasSize(2);
		assertThat(loadedCourses).extracting(Course::getCourseNumber).containsOnlyOnce(courseNumber1);
		assertThat(loadedCourses).extracting(Course::getCourseNumber).containsOnlyOnce(courseNumber2);
	}

	@Test
	public void findByName_Success_EmptyResult() {
		// [GIVEN]
		String courseName = "Not-Existing";

		// [WHEN]
		List<Course> loadedCourses = courseRepository.findByName(courseName);

		// [THEN]
		assertThat(loadedCourses).isEmpty();
	}

	@Test
	public void findByParticipantsContaining_Success() {
		// [GIVEN]
		Customer customer = customer1;
		CourseNumber courseNumber1 = course1.getCourseNumber();
		CourseNumber courseNumber2 = course2.getCourseNumber();

		// [WHEN]
		List<Course> loadedCourses = courseRepository.findByParticipantsContaining(customer);

		// [THEN]
		assertThat(loadedCourses).hasSize(2);
		assertThat(loadedCourses).extracting(Course::getCourseNumber).containsOnlyOnce(courseNumber1);
		assertThat(loadedCourses).extracting(Course::getCourseNumber).containsOnlyOnce(courseNumber2);
	}

	@Test
	public void findByParticipantsContaining_Success_EmptyResult() {
		// [GIVEN]
		Customer customer = customer2;

		// [WHEN]
		List<Course> loadedCourses = courseRepository.findByParticipantsContaining(customer);

		// [THEN]
		assertThat(loadedCourses).isEmpty();
	}

	@Test
	public void deleteByCourseNumber_Success() {
		// [GIVEN]
		CourseNumber courseNumber = course1.getCourseNumber();

		// [WHEN]
		courseRepository.deleteByCourseNumber(courseNumber);

		// [THEN]
		Optional<Course> loadedCourse = courseRepository.findByCourseNumber(courseNumber);
		assertThat(loadedCourse.isPresent()).isFalse();
	}

	@Test
	public void deleteByCourseNumber_Success_NoActualDeletion() {
		// [GIVEN]
		CourseNumber courseNumber = new CourseNumber("0000");

		// [WHEN]
		courseRepository.deleteByCourseNumber(courseNumber);

		// [THEN]
		List<Course> loadedCourses = courseRepository.findAll();
		assertThat(loadedCourses).hasSize(2);
	}

}
