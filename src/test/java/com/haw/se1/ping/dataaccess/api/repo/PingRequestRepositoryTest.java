package com.haw.se1.ping.dataaccess.api.repo;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;

/**
 * Test class for {@link PingRequestRepository}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
public class PingRequestRepositoryTest {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingRequestRepository pingRequestRepository;

	@BeforeEach // causes this method to be executed before each test method execution
	public void setUp() {
		// set up fresh test data before each test method execution

		PingRequest pingRequest = new PingRequest("localhost", new Date());
		pingRequestRepository.save(pingRequest);
	}

	@AfterEach // causes this method to be executed after each test method execution
	public void tearDown() {
		// clean up test data after each test method execution

		pingRequestRepository.deleteAll();
	}

	// No custom methods to test (methods inherited from JpaRepository don't need to be tested)

}
