package com.haw.se1.ping.facade.api;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.ping.dataaccess.api.repo.PingRequestRepository;

import io.restassured.RestAssured;

/**
 * Test class for {@link PingFacade}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
public class PingFacadeTest {

	@LocalServerPort // causes the field to be initialized with the local server port used for REST calls
	private int port;

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingRequestRepository pingRequestRepository;

	@BeforeEach // causes this method to be executed before each test method execution
	public void setUp() {
		// set up fresh test data before each test method execution

		RestAssured.port = port;
		RestAssured.basePath = "";
	}

	@AfterEach // causes this method to be executed after each test method execution
	public void tearDown() {
		// clean up test data after each test method execution

		pingRequestRepository.deleteAll();
	}

	@Test // marks this method as a test case
	public void ping() {
		// @formatter:off
		// [GIVEN]
		given()

		// [WHEN]
		.when()
		.get("/ping")

		// [THEN]
		.then()
		.statusCode(HttpStatus.OK.value())
		.body("responder", equalTo("localhost"));
		// @formatter:on
	}

}
