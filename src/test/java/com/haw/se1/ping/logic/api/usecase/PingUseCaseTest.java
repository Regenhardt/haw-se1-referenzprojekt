package com.haw.se1.ping.logic.api.usecase;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.haw.se1.OnlineCourseApplication;
import com.haw.se1.ping.common.api.datatype.PingResponse;
import com.haw.se1.ping.dataaccess.api.entity.PingRequest;
import com.haw.se1.ping.dataaccess.api.repo.PingRequestRepository;

/**
 * Test class for {@link PingUseCase}.
 * 
 * @author Arne Busch
 */
@SpringBootTest(classes = OnlineCourseApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE) // environment
@ExtendWith(SpringExtension.class) // required to use Spring TestContext Framework in JUnit 5
@ActiveProfiles("test") // causes exclusive creation of general and test-specific beans (marked by @Profile("test"))
public class PingUseCaseTest {

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingUseCase pingUseCase;

	@Autowired // automatically initializes the field with a Spring bean of a matching type
	private PingRequestRepository pingRequestRepository;

	@AfterEach // causes this method to be executed after each test method execution
	public void tearDown() {
		// clean up test data after each test method execution

		pingRequestRepository.deleteAll();
	}

	@Test // marks this method as a test case
	public void ping() {
		// [GIVEN]
		PingRequest request = new PingRequest("localhost", new Date());

		// [WHEN]
		PingResponse response = pingUseCase.ping(request);

		// [THEN]
		assertThat(response).isNotNull();
		assertThat(response.getResponder()).isNotNull();
		assertThat(response.getResponseTime()).isNotNull();

		long numberOfEntities = pingRequestRepository.count();
		assertThat(numberOfEntities).isEqualTo(1);
	}

}
